Parsing Summary
===============
Summary of various parsing techniques. Made it for a university course on compilers.

The LR(0) automaton illustrations are taken from the book "Parsing Techniques - A Practical Guide".

Note: ε denotes the empty string, some sources use λ instead.

